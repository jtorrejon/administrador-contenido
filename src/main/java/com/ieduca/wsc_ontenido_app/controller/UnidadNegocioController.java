package com.ieduca.wsc_ontenido_app.controller;

import com.ieduca.wsc_ontenido_app.model.UnidadNegocio;
import com.ieduca.wsc_ontenido_app.service.UnidadNegocioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("unidadnegocio")
public class UnidadNegocioController {

    private final UnidadNegocioService uneService;

    @Autowired
    public UnidadNegocioController(UnidadNegocioService uneService) {
        this.uneService = uneService;
    }

    @GetMapping("/unidadnegocio")
    public List<UnidadNegocio> getUnidadesDeNegocio() {
        return uneService.getUnidadesDeNegocio();
    }



}
