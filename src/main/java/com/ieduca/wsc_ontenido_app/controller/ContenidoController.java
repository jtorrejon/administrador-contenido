package com.ieduca.wsc_ontenido_app.controller;

import com.ieduca.wsc_ontenido_app.model.ContenidoImg;
import com.ieduca.wsc_ontenido_app.model.request.ContenidoRequest;
import com.ieduca.wsc_ontenido_app.model.response.ContenidoResponse;
import com.ieduca.wsc_ontenido_app.service.ContenidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("contenido")
public class ContenidoController {

    private final ContenidoService contenidoService;

    @Autowired
    public ContenidoController(ContenidoService contenidoService) {
        this.contenidoService = contenidoService;
    }

    @GetMapping("/")
    public List<ContenidoResponse> getContenidos() {
        return contenidoService.getContenido(
                new ContenidoRequest(
                        "CERTUS" ,
                        "otro" ,
                        "otro" ,
                        "otro")
        );
    }

    @PostMapping("/")
    public List<ContenidoResponse> postContenidos(@RequestBody ContenidoRequest body) {
        return contenidoService.getContenido(       body
        );

    }

}
