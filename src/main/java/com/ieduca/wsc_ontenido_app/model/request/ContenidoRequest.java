package com.ieduca.wsc_ontenido_app.model.request;


public class ContenidoRequest {


        private String unidadnegocio;
        private String categoria;
        private String tipocontenido;
        private String gradoAcademico;

    public ContenidoRequest(String unidadnegocio, String categoria, String tipocontenido, String gradoAcademico) {
        this.unidadnegocio = unidadnegocio;
        this.categoria = categoria;
        this.tipocontenido = tipocontenido;
        this.gradoAcademico = gradoAcademico;
    }

    public String getUnidadnegocio() {
        return unidadnegocio;
    }

    public void setUnidadnegocio(String unidadnegocio) {
        this.unidadnegocio = unidadnegocio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return  unidadnegocio  + ","  +  categoria +  "," + tipocontenido + ","+  gradoAcademico ;
    }

    public String getTipocontenido() {
        return tipocontenido;
    }

    public void setTipocontenido(String tipocontenido) {
        this.tipocontenido = tipocontenido;
    }

    public String getGradoAcademico() {
        return gradoAcademico;
    }

    public void setGradoAcademico(String gradoAcademico) {
        this.gradoAcademico = gradoAcademico;
    }
}
