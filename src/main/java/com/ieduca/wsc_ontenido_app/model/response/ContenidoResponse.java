package com.ieduca.wsc_ontenido_app.model.response;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContenidoResponse {

    @Id
    @Column(name = "ContenidoID")
    private Long id;

    @Column(name =  "Titulo")
    private String titulo;

    @Column (name= "Img_url")
    private String img_url;

    @Column (name= "Video_url")
    private String video_url;

    @Column (name = "Mensaje")
    private String mensaje ;

    @Column (name = "Fecha_Mostrar")
    private String fecha_Mostrar ;

    @Column (name = "ContenidoReferenciaID")
    private String contenidoReferenciaID ;

    @Column (name = "ContenidoPrincipalID")
    private String contenidoPrincipalID ;

    @Column (name= "H_Ref")
    private String h_ref;

    @Column (name = "Fecha_Registro")
    private String fecha_Registro ;

    @Column (name = "Fecha_Modifica")
    private String fecha_Modifica ;


    @Column (name= "UnidadNegocio")
    private String unidadNegocio;

    @Column (name= "Categoria")
    private String categoria;
    @Column (name= "TipoContenido")
    private String tipoContenido;
    @Column (name= "GradoAcademico")
    private String gradoAcademico;




    public ContenidoResponse(){

    }

    public ContenidoResponse(Long id, String titulo, String img_url, String video_url, String mensaje, String fecha_Mostrar, String contenidoReferenciaID, String contenidoPrincipalID, String h_ref, String fecha_Registro, String fecha_Modifica, String unidadNegocio, String categoria, String tipoContenido, String gradoAcademico) {
        this.id = id;
        this.titulo = titulo;
        this.img_url = img_url;
        this.video_url = video_url;
        this.mensaje = mensaje;
        this.fecha_Mostrar = fecha_Mostrar;
        this.contenidoReferenciaID = contenidoReferenciaID;
        this.contenidoPrincipalID = contenidoPrincipalID;
        this.h_ref = h_ref;
        this.fecha_Registro = fecha_Registro;
        this.fecha_Modifica = fecha_Modifica;
        this.unidadNegocio = unidadNegocio;
        this.categoria = categoria;
        this.tipoContenido = tipoContenido;
        this.gradoAcademico = gradoAcademico;
    }


    public Long getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getVideo_url() {
        return video_url;
    }

    public String getMensaje() {
        return mensaje;
    }

    public String getFecha_Mostrar() {
        return fecha_Mostrar;
    }

    public String getContenidoReferenciaID() {
        return contenidoReferenciaID;
    }

    public String getContenidoPrincipalID() {
        return contenidoPrincipalID;
    }

    public String getH_ref() {
        return h_ref;
    }

    public String getFecha_Registro() {
        return fecha_Registro;
    }

    public String getFecha_Modifica() {
        return fecha_Modifica;
    }

    public String getUnidadNegocio() {
        return unidadNegocio;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }


    public String getGradoAcademico() {
        return gradoAcademico;
    }

    @Override
    public String toString() {
        return "ContenidoResponse{" +
                "id=" + id +
                ", titulo='" + titulo + '\'' +
                ", img_url='" + img_url + '\'' +
                ", video_url='" + video_url + '\'' +
                ", mensaje='" + mensaje + '\'' +
                ", fecha_Mostrar='" + fecha_Mostrar + '\'' +
                ", contenidoReferenciaID='" + contenidoReferenciaID + '\'' +
                ", contenidoPrincipalID='" + contenidoPrincipalID + '\'' +
                ", h_ref='" + h_ref + '\'' +
                ", fecha_Registro='" + fecha_Registro + '\'' +
                ", fecha_Modifica='" + fecha_Modifica + '\'' +
                ", unidadNegocio='" + unidadNegocio + '\'' +
                ", categoria='" + categoria + '\'' +
                ", tipoContenido='" + tipoContenido + '\'' +
                ", gradoAcademico='" + gradoAcademico + '\'' +
                '}';
    }



}