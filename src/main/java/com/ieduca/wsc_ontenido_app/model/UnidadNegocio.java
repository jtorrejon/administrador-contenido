package com.ieduca.wsc_ontenido_app.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UnidadNegocio {

    @Id
    private Long unidadnegocioid;
    private String nombre;
    private Character activo;

    public UnidadNegocio() {
    }

    public UnidadNegocio(Long unidadnegocioid, String nombre, Character activo) {
        this.unidadnegocioid = unidadnegocioid;
        this.nombre = nombre;
        this.activo = activo;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Character getActivo() {
        return activo;
    }

    public void setActivo(Character activo) {
        this.activo = activo;
    }

    @Override
    public String toString() {
        return "UnidadNegocio{" +
                "id=" + unidadnegocioid +
                ", nombre='" + nombre + '\'' +
                ", activo=" + activo +
                '}';
    }
}
