package com.ieduca.wsc_ontenido_app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContenidoImg {


    //ContenidoID, Titulo, Img_url, H_Ref, Fecha_Registro, Fecha_Modifica, UnidadNegocio, Categoria, TipoContenido, GradoAcademico
    @Id @Column(name = "ContenidoID")
    private Long id;

    @Column(name =  "Titulo")
    private String titulo;

    @Column (name= "Img_url")
    private String img_url;

    @Column (name= "H_Ref")
    private String h_ref;

    @Column (name= "Fecha_Registro")
    private String registro;


    @Column (name= "Fecha_Modifica")
    private String modificacion;

    @Column (name= "UnidadNegocio")
    private String unidadNegocio;

    @Column (name= "Categoria")
    private String categoria;


    @Column (name= "TipoContenido")
    private String tipoContenido;

    @Column (name= "GradoAcademico")
    private String gradoAcademico;


    public ContenidoImg() {
    }

    public ContenidoImg(
            Long id,
            String titulo,
            String img_url,
            String h_ref,
            String registro,
            String modificacion ,
            String unidadNegocio,
            String categoria,
            String tipoContenido,
            String gradoAcademico
    ) {
        this.id = id;
        this.titulo = titulo;
        this.img_url = img_url;
        this.h_ref = h_ref;
        this.registro = registro;
        this.modificacion = modificacion;
        this.unidadNegocio = unidadNegocio;
        this.categoria = categoria;
        this.tipoContenido = tipoContenido;
        this.gradoAcademico = gradoAcademico;
    }

    public Long getId() {
        return id;
    }

    public String getTitulo() {
        return titulo;
    }

    public String getImg_url() {
        return img_url;
    }

    public String getH_ref() {
        return h_ref;
    }

    public String getRegistro() {
        return registro;
    }

    public String getModificacion() {
        return modificacion;
    }

    public String getUnidadNegocio() {
        return unidadNegocio;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getTipoContenido() {
        return tipoContenido;
    }

    public String getGradoAcademico() {
        return gradoAcademico;
    }
}
