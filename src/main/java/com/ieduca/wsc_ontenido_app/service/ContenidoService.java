package com.ieduca.wsc_ontenido_app.service;

import com.ieduca.wsc_ontenido_app.model.ContenidoImg;
import com.ieduca.wsc_ontenido_app.model.request.ContenidoRequest;
import com.ieduca.wsc_ontenido_app.model.response.ContenidoResponse;
import com.ieduca.wsc_ontenido_app.repository.ContenidoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContenidoService {

    private final ContenidoRepository contenidoRepo;

    @Autowired
    public ContenidoService(ContenidoRepository contenidoRepo) {
        this.contenidoRepo = contenidoRepo;
    }

    public List<ContenidoResponse> getContenido( ContenidoRequest request) {
        System.out.println(  " Request :"+ request);
        List<ContenidoResponse> response =  contenidoRepo.getContenido(request) ;

        for (ContenidoResponse resp : response) {
            System.out.println(  "\n R :  " + resp );

        }

        return response;
    }

}
