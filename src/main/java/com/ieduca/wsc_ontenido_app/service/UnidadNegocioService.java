package com.ieduca.wsc_ontenido_app.service;

import com.ieduca.wsc_ontenido_app.model.UnidadNegocio;
import com.ieduca.wsc_ontenido_app.repository.UnidadNegocioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UnidadNegocioService {

    private final UnidadNegocioRepository uneRepository;

    @Autowired
    public UnidadNegocioService(UnidadNegocioRepository studentRepository) {
        this.uneRepository = studentRepository;
    }

    public List< UnidadNegocio> getUnidadesDeNegocio() {
        return uneRepository.findAll();
    }

}
