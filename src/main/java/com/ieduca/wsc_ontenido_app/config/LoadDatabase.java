package com.ieduca.wsc_ontenido_app.config;

import com.ieduca.wsc_ontenido_app.repository.UnidadNegocioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(UnidadNegocioRepository repository) {
        return args -> {
            //log.info("Preloading " + repository.save(new UnidadNegocio(2L, "CERTUS" , 'N'  )));
            //log.info("Preloading " + repository.save(new UnidadNegocio(3L, "TSL" , 'S'  )));
            //log.info("Preloading " + repository.save(new UnidadNegocio(1L, "UCAL" , 'S'  )));
        };
    }
}
