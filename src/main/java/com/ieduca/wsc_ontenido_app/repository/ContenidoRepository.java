package com.ieduca.wsc_ontenido_app.repository;


import com.ieduca.wsc_ontenido_app.model.ContenidoImg;
import com.ieduca.wsc_ontenido_app.model.request.ContenidoRequest;
import com.ieduca.wsc_ontenido_app.model.response.ContenidoResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ContenidoRepository extends JpaRepository<ContenidoResponse, Long> {
    @Query(value =  "call SP_CONTENIDO_LISTAR(" +
            " :#{#request.unidadnegocio}  ," +
            " :#{#request.categoria}  , " +
            " :#{#request.tipocontenido} , " +
            " :#{#request.gradoAcademico} " +
            "  );" , nativeQuery = true)
    List<ContenidoResponse> getContenido(
            @Param("request")
            ContenidoRequest request
    );

}
