package com.ieduca.wsc_ontenido_app.repository;

import com.ieduca.wsc_ontenido_app.model.UnidadNegocio;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UnidadNegocioRepository extends JpaRepository<UnidadNegocio, Long> {

}
