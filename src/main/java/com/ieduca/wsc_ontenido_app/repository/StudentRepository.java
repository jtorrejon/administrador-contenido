package com.ieduca.wsc_ontenido_app.repository;

import com.ieduca.wsc_ontenido_app.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Long> {
}
