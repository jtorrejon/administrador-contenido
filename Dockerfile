#
# Build stage
#
FROM maven:3.8.5-eclipse-temurin-11-alpine AS build

COPY src /home/certus/src/
COPY pom.xml /home/certus/

WORKDIR /home/certus

RUN mvn clean install
RUN mvn package



#
# Certus launch
#
FROM eclipse-temurin:11-alpine AS app
COPY --from=build /home/certus/target/*.jar /opt/app/app.jar
CMD ["java","-jar","/opt/app/app.jar"]